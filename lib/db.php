<?php 
	global $conn;
	function connect_db(){
		global $conn;
		if(!$conn){
			try {
				$conn = new PDO("mysql:host=localhost;dbname=vne;charset=utf8",'root','');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e) {
				die("Failed to connect DB");
			}
		}
	}
	function disconnect_db(){
		global $conn;
		$conn = NULL;
	}
	function get_contents(){
		global $conn;
		connect_db();
		$stmt = $conn->prepare("SELECT * FROM content");
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$result = $stmt->fetchAll();
		return $result;
	}
	function is_exist($link){
		global $conn;
		connect_db();
		$stmt= $conn->prepare("SELECT * FROM `content` WHERE `link` LIKE :link");
		$stmt->bindValue(':link','%' . $link . '%');
		$stmt->execute();
		if($stmt->rowCount() == 0) return false;
		else return true;
	}
	function add_content($item){
		global $conn;
		connect_db();
		$stmt = $conn->prepare("INSERT INTO content(link,title,brief,description) VALUES (?,?,?,?)");
		$stmt->bindParam(1,$item['link']);
		$stmt->bindParam(2,$item['title']);
		$stmt->bindParam(3,$item['brief']);
		$stmt->bindParam(4,$item['description']);
		$stmt->execute();
	}
	function remove_emoji($string) {

	    // Match Emoticons
	    $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
	    $clear_string = preg_replace($regex_emoticons, '', $string);

	    // Match Miscellaneous Symbols and Pictographs
	    $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
	    $clear_string = preg_replace($regex_symbols, '', $clear_string);

	    // Match Transport And Map Symbols
	    $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
	    $clear_string = preg_replace($regex_transport, '', $clear_string);

	    // Match Miscellaneous Symbols
	    $regex_misc = '/[\x{2600}-\x{26FF}]/u';
	    $clear_string = preg_replace($regex_misc, '', $clear_string);

	    // Match Dingbats
	    $regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
	    $clear_string = preg_replace($regex_dingbats, '', $clear_string);

	    return $clear_string;
	}
	function get_content_genk($link){
		$html = file_get_html($link);
		$title_pattern = "h1.kbwc-title";
		$brief_pattern = "h2.knc-sapo";
		$description_pattern = "div.knc-content";
		$description_pattern_edit = "div.VCSortableInPreviewMode,a.link-inline-content";
		$description_pattern_remove = "br,i";
		$item=array();
		$item['link'] = $link;
	    $item['title'] = trim($html->find($title_pattern)[0]->plaintext);
	    $item['brief'] = trim($html->find($brief_pattern)[0]->plaintext);
	    $item['description']='';
		foreach($html->find($description_pattern) as $e1){
			if($description_pattern_edit){
                    $arr = explode(',',$description_pattern_edit);
                    for($i=0;$i<count($arr);$i++){
                        foreach($e1->find($arr[$i]) as $e2){
                        	if($e2->find("img")){
                        		$img_src = $e2->find("img")[0]->src;
                        		$e2->outertext = '<img src="' . $img_src . '"/>';
                        		continue;
                        	}
                        	else {
                        		if($e2->find("a")){
                        			$e2->outertext = '';
                        			continue;
                        		}
                        		$e2->outertext = $e2->innertext;
                        	}
                        	
                        }
                    }
            }
            if($description_pattern_remove){
            	$arr = explode(',',$description_pattern_remove);
            	for ($i=0; $i < count($arr); $i++) { 
            		foreach ($e1->find($arr[$i]) as $e2) {
            			$e2->outertext=$e2->innertext;
            		}
            	}
            }
            foreach ($e1->find("p") as $e2) {
            	$e2->outertext = '<p>' . $e2->innertext . '</p>';
            }
	        $item['description'] = remove_emoji($e1->innertext); // Lấy toàn bộ phần html
		}
		return $item;
	}
	function get_content_zing($link){
		$data = file_get_contents($link);
		$data = gzinflate(substr($data, 10, -8));
		$html = str_get_html($data);
		$title_pattern = "h1.the-article-title";
		$brief_pattern = "p.the-article-summary";
		$description_pattern = "div.the-article-body";
		$description_pattern_remove = "br,i,em,abbr,coccocgrammar,blockquote";
		$item=array();
		$item['link'] = $link;
	    $item['title'] = $html->find($title_pattern)[0]->innertext;
	    $item['brief'] = $html->find($brief_pattern)[0]->innertext;
	    $item['description']='';
	    // e[number] presents for level of element
		foreach($html->find($description_pattern) as $e1){
			//Remove related article
			if($e1->find("table.article"))
				$e1->find("table.article")[0]->outertext = '';
			//Remove notebox
			foreach ($e1->find("div.notebox") as $e2) {
				$e2->outertext = '';
			}
			//Remove autolink
			foreach ($e1->find("a.autolink") as $e2) {
				$e2->outertext = $e2->innertext;
			}
			//Remove header tag
			foreach ($e1->find("h1") as $e2) {
				if($e2->find("img")){
					$e2->outertext = '<img src="' . $e2->find("img")[0]->src . '"/>';
				}
				else {
					$e2->outertext = '<p>' . $e2->plaintext . '</p>';
				}
			}
			foreach ($e1->find("h2") as $e2) {
				if($e2->find("img")){
					$e2->outertext = '<img src="' . $e2->find("img")[0]->src . '"/>';
				}
				else {
					$e2->outertext = '<p>' . $e2->plaintext . '</p>';
				}
			}
			foreach ($e1->find("h3") as $e2) {
				if($e2->find("img")){
					$e2->outertext = '<img src="' . $e2->find("img")[0]->src . '"/>';
				}
				else {
					$e2->outertext = '<p>' . $e2->plaintext . '</p>';
				}
			}
			//Remove additional tag
			if($description_pattern_remove){
            	$arr = explode(',',$description_pattern_remove);
            	for ($i=0; $i < count($arr); $i++) { 
            		foreach ($e1->find($arr[$i]) as $e2) {
            			$e2->outertext=$e2->innertext;
            		}
            	}
            }
			//Handling with author
			if($e1->find("div.wiki-author")){
				$e1->find("div.wiki-author")[0]->outertext = '';
			}
			
			//Handling with picture 
			if(strpos($e1->parent()->parent()->class, "type-picture")){
				foreach ($e1->find("table.picture") as $e2) {
					if($e2->plaintext){
						$img_src = '<img src="' . $e2->find("img")[0]->src . '"/>';
						if($e2->find("td.pCaption"))
						$desc = "<p>" . $e2->find("td.pCaption")[0]->innertext . "</p>";
						$e2->outertext = $img_src . $desc;
					}
					else{
						$e2->outertext = '';
					}
				}
			}
			else{
				foreach ($e1->find("table.picture") as $e2) {
					$img_src = '<img src="' . $e2->find("img")[0]->src . '"/>';
					$e2->outertext = $img_src;
				}
			}
			
			$item['description'] = $e1->innertext;
		}
		return $item;
	}
?>